﻿$(function () {
    "use string";

    window.SelectMesDefinido = window.SelectMesDefinido || {};

    SelectMesDefinido.Carregar = function () {
        $(document).on("change", "#KeyMes", function () {
            var mes = $("#KeyMes").val() === null ? $("#graficopizzasaida").data("mes") : $("#KeyMes").val();
            var ano = $("#KeyAno").val() === null ? $("#graficogeralporano").data("anoinicio") : $("#KeyAno").val();
            GraficoPizzaEntrada.BuscarInformacoes(ano, mes)
            GraficoPizzaSaida.BuscarInformacoes(ano, mes)
        });
    };

    SelectMesDefinido.Carregar();
});