﻿$(function () {

    "use scripts";

    window.FiltroFinanceiro = window.FiltroFinanceiro || {};

    var divQueCarregaItens = $(".carregar-itens");

    FiltroFinanceiro.CarregarEvento = function () {
        $(document).on("keyup", "#input-pesquisa", function () {
            FiltroFinanceiro.Get();
        });

        $(document).on("change", "#fin-tipo", function () {
            FiltroFinanceiro.Get();
        });

        $(document).on("change", "#fin-cate", function () {
            FiltroFinanceiro.Get();
        });

        $(document).on("change", "#fin-data", function () {
            FiltroFinanceiro.Get();
        });

        $(document).on("change", "#fin-ano", function () {
            FiltroFinanceiro.Get();
        });

        $(document).on("click", "#pageLink a[href]", function () {
            divQueCarregaItens.LoadingOverlay("show");
            var categoria = $("#fin-cate").val();
            var tipo = $("#fin-tipo").val();
            var data = $("#fin-data").val();
            var ano = $("#fin-ano").val() === null ? $("#fin-ano").data("ano") : $("#fin-ano").val();
            var pesquisa = $("#input-pesquisa").val();
            var paginaAtual = $("#pageLink").data("paginaatual");
            var paginaHref = $(this).attr("href");
            var pg = FiltroFinanceiro.TipoHaSerBuscado(paginaHref);
            var pagina = $(this).text();
            switch (pagina) {
                case "»":
                    pagina = paginaAtual + 1;
                    break;
                case "«":
                    pagina = paginaAtual - 1;
                    break;
                case "»»":
                    pagina = pg;
                    break;
                case "««":
                    pagina = pg;
                    break;
                default:
            }
            FiltroFinanceiro.Pesquisar(categoria, tipo, data, ano, pesquisa, pagina);
            return false;
        });
    };

    FiltroFinanceiro.Pesquisar = function (categoria, tipo, data, ano, pesquisa, pagina = 1) {
        var url = $(".carregar-itens").data("url");
        $.ajax({
            url: url,
            data: { textoDigitado: pesquisa, categoria, tipo, data, ano, pagina },
            method: "POST",
            success: function (result) {
                divQueCarregaItens.empty();
                divQueCarregaItens.append(result);
                Mask.Values();
                divQueCarregaItens.LoadingOverlay("hide");
            },
            error: function () {
                Lobibox.notify('error', { position: 'top right', sound: '../../sounds/sound4', delay: 3000, msg: 'Erro ao pesquisar informação', title: 'Erro', icon: 'frown icon' });
                divQueCarregaItens.LoadingOverlay("hide");
            }
        });
    };

    FiltroFinanceiro.TipoHaSerBuscado = function (paginaHref) {
        return paginaHref.replace(/\D/g, '');
    };

    FiltroFinanceiro.Get = function () {
        divQueCarregaItens.LoadingOverlay("show");
        var categoria = $("#fin-cate").val();
        var tipo = $("#fin-tipo").val();
        var data = $("#fin-data").val();
        var ano = $("#fin-ano").val() === null ? $("#fin-ano").data("ano") : $("#fin-ano").val();
        var pesquisa = $("#input-pesquisa").val();
        FiltroFinanceiro.Pesquisar(categoria, tipo, data, ano, pesquisa, 1);
    };

    FiltroFinanceiro.CarregarEvento();
});