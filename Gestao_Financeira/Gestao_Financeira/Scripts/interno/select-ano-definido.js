﻿$(function () {
    "use string";

    window.SelectAnoDefinido = window.SelectAnoDefinido || {};

    SelectAnoDefinido.Carregar = function () {
        $(document).on("change", "#KeyAno", function () {
            var mes = $("#KeyMes").val() === null ? $("#graficopizzasaida").data("mes") : $("#KeyMes").val();
            var ano = $("#KeyAno").val() === null ? $("#graficogeralporano").data("anoinicio") : $("#KeyAno").val();
            SelectAnoDefinido.BuscarInformacoesPainel(ano);
            GraficoGeralPorMes.BuscarInformacoes(ano);
            GraficoPizzaEntrada.BuscarInformacoes(ano, mes)
            GraficoPizzaSaida.BuscarInformacoes(ano, mes)
        });
    };

    SelectAnoDefinido.BuscarInformacoesPainel = function (ano) {
        $.ajax({
            url: $("#div-painel").data("url"),
            data: { ano: ano },
            success: function (result) {
                $("#div-painel").empty();
                $("#div-painel").append(result);
                Mask.Values();
            }
        });
    };

    SelectAnoDefinido.Carregar();
});