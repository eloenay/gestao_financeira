﻿$(function () {
    "use string";

    window.GraficoGeralPorAno = window.GraficoGeralPorAno || {};

    GraficoGeralPorAno.Carregar = function () {
        $(document).ready(function () {
            GraficoGeralPorAno.BuscarInformacoes();
        });
    };

    GraficoGeralPorAno.BuscarInformacoes = function (ano = null) {
        var graficoAno = $("#graficogeralporano");
        graficoAno.LoadingOverlay("show");
        $.ajax({
            url: graficoAno.data("url"),
            data: { ano: ano },
            success: function (result) {
                GraficoGeralPorAno.MontarGrafico(result);
                graficoAno.LoadingOverlay("hide");
            }
        });
    };

    GraficoGeralPorAno.MontarGrafico = function (result) {
        Highcharts.chart('graficogeralporano', {
            title: {
                text: 'Gráficos de valores anuais'
            },

            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            },

            yAxis: {
                title: {
                    text: 'Valores monetários'
                }
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: $("#graficogeralporano").data("anoinicio")
                }
            },

            series: result,
        });
    };

    GraficoGeralPorAno.Carregar();
});