﻿$(function () {
    "use string";

    window.GraficoGeralPorMes = window.GraficoGeralPorMes || {};

    GraficoGeralPorMes.Carregar = function () {
        $(document).ready(function () {
            GraficoGeralPorMes.BuscarInformacoes();
        });
    };

    GraficoGeralPorMes.BuscarInformacoes = function (ano = null) {
        var graficoMes = $("#graficogeralpormes");
        graficoMes.LoadingOverlay("show");
        $.ajax({
            url: graficoMes.data("url"),
            data: { ano: ano },
            success: function (result) {
                GraficoGeralPorMes.MontarGrafico(result);
                graficoMes.LoadingOverlay("hide");
            }
        });
    };

    GraficoGeralPorMes.MontarGrafico = function (result) {
        Highcharts.chart('graficogeralpormes', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Gráficos de valores mensais por ano definido'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Fev',
                    'Mar',
                    'Abr',
                    'Mai',
                    'Jun',
                    'Jul',
                    'Ago',
                    'Set',
                    'Out',
                    'Nov',
                    'Dez'
                ],
                crosshair: true
            },
            yAxis: {
                title: {
                    text: 'Valores monetários'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>R$ {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },

            series: result
        });
    };

    GraficoGeralPorMes.Carregar();
});