﻿using System;

namespace Gestao_Financeira.Utils
{
    public static class StringFormats
    {
        public static string MarkValor(this decimal valor)
        {
            return String.Format("R$ {0:N}", valor);
        }
    }
}