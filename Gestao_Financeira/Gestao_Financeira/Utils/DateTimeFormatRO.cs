﻿using System;
using System.Runtime.InteropServices;

namespace Gestao_Financeira.Utils
{
    public static class DateTimeFormatRO
    {
        public static DateTime GetDateTimeFusoRondonia(DateTime data)
        {
            string timeZoneId = RuntimeInformation.IsOSPlatform(OSPlatform.Windows)
    ? "SA Western Standard Time"
    : "America/Porto_Velho";
            return TimeZoneInfo.ConvertTimeFromUtc(data, TimeZoneInfo.FindSystemTimeZoneById(timeZoneId));
        }
    }
}