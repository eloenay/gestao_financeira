﻿using Gestao_Financeira.Helpers;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Gestao_Financeira.Areas.User.Controllers
{
    [Permissoes(Roles = "Usuario")]
    public class FinanceiroController : Controller
    {

        private readonly Financeiro _financeiro;
        private readonly Categoria _categoria;

        public FinanceiroController()
        {
            _financeiro = new Financeiro();
            _categoria = new Categoria();
        }

        public ActionResult Index(int pagina = 1)
        {
            try
            {
                if (Account.UsuarioId == Guid.Empty)
                    return RedirectToAction("Index", "Home");

                var listaDeAnos = _financeiro.BuscarTodos(Account.UsuarioId).GroupBy(x => x.DataLancamento.Year).OrderBy(x => x.Key).ToList();

                ViewBag.SelectAnos = new SelectList(listaDeAnos, "Key", "Key");
                ViewBag.Categorias = new SelectList(_categoria.BuscarTodos(Account.UsuarioId).Where(x => !x.Excluido && x.Ativo), "CategoriaId", "Nome");
                TempData["Pagina"] = pagina;
                int itensPorPagina = 5;
                var financeiros = _financeiro.BuscarTodos(Account.UsuarioId).ToList();
                var lista = new List<Financeiro>();
                lista.AddRange(financeiros);
                return View(lista.ToPagedList(pagina, itensPorPagina));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Adicionar(Financeiro financeiro, string valor)
        {
            if (string.IsNullOrEmpty(financeiro.Descricao) || string.IsNullOrEmpty(valor) || financeiro.TipoFinanceiro == 0)
                return View().Error("Erro ao adicionar financeiro");
            financeiro.DataLancamento = DateTimeFormatRO.GetDateTimeFusoRondonia(financeiro.DataLancamento.AddDays(1));
            financeiro.Valor = Convert.ToDecimal(valor);
            financeiro.Adicionar(financeiro);
            return RedirectToAction("Index", new { area = "User" }).Success("Financeiro cadastrado com sucesso.");
        }

        [HttpPost]
        public ActionResult Atualizar(Financeiro financeiro, string valor)
        {
            if (string.IsNullOrEmpty(financeiro.Descricao) || string.IsNullOrEmpty(valor) || financeiro.TipoFinanceiro == 0)
                return View().Error("Erro ao atualizar financeiro");
            financeiro.EditarFinanceiro(valor);
            _financeiro.Atualizar(financeiro);
            return RedirectToAction("Index", new { area = "User" }).Success("Financeiro atualizado com sucesso.");
        }

        public PartialViewResult AbrirModalAdicionar()
        {
            ViewBag.Categorias = new SelectList(_categoria.BuscarTodos(Account.UsuarioId).Where(x => !x.Excluido && x.Ativo), "CategoriaId", "Nome");
            return PartialView("_Adicionar");
        }

        public PartialViewResult AbrirModalAtualizar(Guid financeiroId)
        {
            ViewBag.Categorias = new SelectList(_categoria.BuscarTodos(Account.UsuarioId).Where(x => !x.Excluido && x.Ativo), "CategoriaId", "Nome");
            var financeiro = _financeiro.BuscarPorId(financeiroId);
            return PartialView("_Atualizar", financeiro);
        }

        public PartialViewResult AbrirModalDetalhes(Guid financeiroId)
        {
            return PartialView("_Detalhes", _financeiro.BuscarPorId(financeiroId));
        }

        public PartialViewResult BuscarFinanceiro(string textoDigitado, string categoria, string tipo = "", int data = 0, int ano = 0, int pagina = 1)
        {
            ano = ano != 0 ? ano : DateTime.UtcNow.Year;
            var itensPorPagina = 5;
            var categoriaId = string.IsNullOrEmpty(categoria) ? Guid.Empty : Guid.Parse(categoria);
            TempData["Pagina"] = pagina;
            var financeiros = _financeiro.BuscarTodosPorFiltro(textoDigitado, categoriaId, tipo, data, ano, Account.UsuarioId);
            var lista = new List<Financeiro>();
            lista.AddRange(financeiros);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }
    }
}