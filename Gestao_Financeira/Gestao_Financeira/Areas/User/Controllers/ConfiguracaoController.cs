﻿using Gestao_Financeira.Helpers;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using System;
using System.Web.Mvc;

namespace Gestao_Financeira.Areas.User.Controllers
{
    [Permissoes(Roles = "Usuario")]
    public class ConfiguracaoController : Controller
    {
        private readonly Usuario _usuario;

        public ConfiguracaoController()
        {
            _usuario = new Usuario();
        }

        public ActionResult AlterarDados()
        {
            try
            {
                if (Account.UsuarioId == Guid.Empty)
                    return RedirectToAction("Index", "Home");

                return View(_usuario.BuscarPorId(Account.UsuarioId));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult AlterarDados(Usuario usuario)
        {
            var usuarioAtual = _usuario.BuscarPorId(usuario.UsuarioId);
            usuarioAtual.AlterarUsuario(usuario);
            _usuario.Atualizar(usuarioAtual);
            return RedirectToAction("Index", "Dashboard", new { area = "User" }).Success();
        }
    }
}