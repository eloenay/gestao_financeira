﻿using Gestao_Financeira.Helpers;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Gestao_Financeira.Areas.User.Controllers
{
    [Permissoes(Roles = "Usuario")]
    public class CategoriaController : Controller
    {

        private readonly Categoria _categoria;

        public CategoriaController()
        {
            _categoria = new Categoria();
        }

        public ActionResult Index(int pagina = 1)
        {
            try
            {
                if (Account.UsuarioId == Guid.Empty)
                    return RedirectToAction("Index", "Home");

                TempData["Pagina"] = pagina;
                int itensPorPagina = 5;
                var categorias = _categoria.BuscarTodos(Account.UsuarioId).Where(x => !x.Excluido).ToList();
                var lista = new List<Categoria>();
                lista.AddRange(categorias);
                return View(lista.ToPagedList(pagina, itensPorPagina));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Adicionar(Categoria categoria)
        {
            if (!ModelState.IsValid) return RedirectToAction("Index", new { area = "User" }).Error("Erro ao cadastrar categoria.");
            categoria.Adicionar(categoria);
            return RedirectToAction("Index", new { area = "User" }).Success("Categoria cadastrado com sucesso.");
        }

        [HttpPost]
        public ActionResult Atualizar(Categoria categoria)
        {
            var categoriaAtual = _categoria.BuscarPorId(categoria.CategoriaId);
            categoriaAtual.AlterarCategoria(categoria);
            _categoria.Atualizar(categoriaAtual);
            return RedirectToAction("Index", new { area = "User" }).Success("Categoria atualizado com sucesso.");
        }

        [HttpPost]
        public ActionResult Excluir(Categoria categoria)
        {
            var categoriaAtual = _categoria.BuscarPorId(categoria.CategoriaId);
            categoriaAtual.ExcluirCategoria();
            _categoria.Excluir(categoriaAtual);
            return RedirectToAction("Index", new { area = "User" }).Success("Categoria excluído com sucesso.");
        }

        public PartialViewResult AbrirModalAdicionar()
        {
            return PartialView("_Adicionar");
        }

        public PartialViewResult AbrirModalAtualizar(Guid categoriaId)
        {
            var categoria = _categoria.BuscarPorId(categoriaId);
            return PartialView("_Atualizar", categoria);
        }

        public PartialViewResult AbrirModalExcluir(Guid categoriaId)
        {
            var categoria = _categoria.BuscarPorId(categoriaId);
            return PartialView("_Excluir", categoria);
        }

        public ActionResult Ativo(Guid categoriaId, int pagina)
        {
            var categoriaAtual = _categoria.BuscarPorId(categoriaId);
            categoriaAtual.AtivarOuDesativar();
            _categoria.Atualizar(categoriaAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var categorias = _categoria.BuscarTodos(Account.UsuarioId).Where(x => !x.Excluido).ToList(); ;
            var lista = new List<Categoria>();
            lista.AddRange(categorias);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarCategoria(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var categorias = _categoria.BuscarTodosPorFiltro(textoDigitado, Account.UsuarioId).Where(x => !x.Excluido).ToList();
            var lista = new List<Categoria>();
            lista.AddRange(categorias);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public JsonResult AdicionarPorFinanceiro(string nome)
        {
            var categoria = new Categoria{ Nome = nome };
            _categoria.Adicionar(categoria);
            return Json(_categoria.BuscarTodos(Account.UsuarioId).Where(x => !x.Excluido).Select(x => $"<option value='{x.CategoriaId}'>{x.Nome}</option>"), JsonRequestBehavior.AllowGet);
        }
    }
}