﻿using Gestao_Financeira.Helpers;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Gestao_Financeira.Areas.User.Controllers
{
    [Permissoes(Roles = "Usuario")]
    public class DashboardController : Controller
    {
        private readonly Financeiro _financeiro;

        public DashboardController()
        {
            _financeiro = new Financeiro();
        }

        public ActionResult Index()
        {
            try
            {
                if(Account.UsuarioId == Guid.Empty)
                    return RedirectToAction("Index", "Home");

                var listaDeAnos = _financeiro.BuscarTodos(Account.UsuarioId).GroupBy(x => x.DataLancamento.Year).OrderBy(x => x.Key).ToList();

                ViewBag.SelectAnos = new SelectList(listaDeAnos, "Key", "Key");
                ViewBag.DataInicial = listaDeAnos.Count() != 0 ? listaDeAnos.FirstOrDefault().Key : DateTime.UtcNow.Year;
                return View(_financeiro.BuscarTodosPorAnoMes(Account.UsuarioId, DateTime.UtcNow.Year, null));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

         public ActionResult CarregarPainelPorAno(int ano)
        {
            return PartialView("_Painel", _financeiro.BuscarTodosPorAnoMes(Account.UsuarioId, ano, null));
        }

        public JsonResult GraficoGeralPorAno()
        {
            var entrada = new List<decimal>();
            var saida = new List<decimal>();
            var saldo = new List<decimal>();
            var result = _financeiro.BuscarTodos(Account.UsuarioId).GroupBy(x => x.DataLancamento.Year).OrderBy(x => x.Key).ToList();
            foreach (var item in result)
            {
                entrada.Add(item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Entrada).Sum(x => x.Valor));
                saida.Add(item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Saida).Sum(x => x.Valor));
                saldo.Add(item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Entrada).Sum(x => x.Valor) - item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Saida).Sum(x => x.Valor));
            }
            var list = new[] {
                    new { name = "Saldo" , data = saldo },
                    new { name = "Saída" , data = saida },
                    new { name = "Entrada" , data = entrada }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GraficoGeralPorMes(int ano = 0)
        {
            ano = ano != 0 ? ano : DateTime.UtcNow.Year;
            var entrada = new List<decimal>();
            var saida = new List<decimal>();
            var saldo = new List<decimal>();
            var key = new List<int>();
            var financeiros = _financeiro.BuscarTodosPorAnoMes(Account.UsuarioId, ano, null).GroupBy(x => x.DataLancamento.Year).OrderBy(x => x.Key).ToList();
            foreach (var itens in financeiros)
            {
                var result = itens.GroupBy(x => x.DataLancamento.Month).ToList();
                VerificarMesNulo(result, entrada, saida, saldo);
            }
            var list = new[] {
                    new { name = "Saldo" , data = saldo },
                    new { name = "Saída" , data = saida },
                    new { name = "Entrada" , data = entrada }
            };
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        private void VerificarMesNulo(List<IGrouping<int, Financeiro>> result, List<decimal> entrada, List<decimal> saida, List<decimal> saldo)
        {
            for (int i = 1; i <= 12; i++)
            {
                if (result.Any(x => x.Key == i))
                {
                    foreach (var item in result.Where(x => x.Key == i))
                    {
                        entrada.Add(item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Entrada).Sum(x => x.Valor));
                        saida.Add(item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Saida).Sum(x => x.Valor));
                        saldo.Add(item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Entrada).Sum(x => x.Valor) - item.Where(x => x.TipoFinanceiro == TipoFinanceiro.Saida).Sum(x => x.Valor));
                    }
                }
                else
                {
                    entrada.Add(0);
                    saida.Add(0);
                    saldo.Add(0);
                }
            }
        }

        public JsonResult GraficoPizzaSaida(int mes = 0, int ano = 0)
        {
            ano = ano != 0 ? ano : DateTime.UtcNow.Year;
            mes = mes != 0 ? mes : DateTime.UtcNow.Month;
            var result = _financeiro.BuscarTodosPorAnoMes(Account.UsuarioId, ano, mes).Where(x => x.TipoFinanceiro == TipoFinanceiro.Saida).ToList().GroupBy(x => x.Categoria.Nome).Select(x => new
            {
                name = x.FirstOrDefault().Categoria.Nome,
                y = x.Sum(c => c.Valor)
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GraficoPizzaEntrada(int mes = 0, int ano = 0)
        {
            ano = ano != 0 ? ano : DateTime.UtcNow.Year;
            mes = mes != 0 ? mes : DateTime.UtcNow.Month;
            var result = _financeiro.BuscarTodosPorAnoMes(Account.UsuarioId, ano, mes).Where(x => x.TipoFinanceiro == TipoFinanceiro.Entrada).ToList().GroupBy(x => x.Categoria.Nome).Select(x => new
            {
                name = x.FirstOrDefault().Categoria.Nome,
                y = x.Sum(c => c.Valor)
            });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}