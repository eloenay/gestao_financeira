﻿using PagedList;
using Gestao_Financeira.Helpers;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Gestao_Financeira.Areas.Admin.Controllers
{
    [Permissoes(Roles = "Administrador")]
    public class UsuarioController : Controller
    {

        private readonly Usuario _usuario;

        public UsuarioController()
        {
            _usuario = new Usuario();
        }

        public ActionResult Index(int pagina = 1)
        {
            try
            {
                if (Account.UsuarioId == Guid.Empty)
                    return RedirectToAction("Index", "Home");

                TempData["Pagina"] = pagina;
                int itensPorPagina = 5;
                var usuarios = _usuario.BuscarTodos().Where(x => !x.Excluido).ToList();
                var lista = new List<Usuario>();
                lista.AddRange(usuarios);
                return View(lista.ToPagedList(pagina, itensPorPagina));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Adicionar(Usuario usuario)
        {
            if (!ModelState.IsValid) return RedirectToAction("Index", new { area = "Admin" }).Error("Erro ao cadastrar usuário.");
            usuario.Senha = Hash.GerarHash(usuario.Senha);
            usuario.Adicionar(usuario);
            return RedirectToAction("Index", new { area = "Admin" }).Success("Usuário cadastrado com sucesso.");
        }

        [HttpPost]
        public ActionResult Atualizar(Usuario usuario)
        {
            var usuarioAtual = _usuario.BuscarPorId(usuario.UsuarioId);
            usuarioAtual.AlterarUsuario(usuario);
            _usuario.Atualizar(usuarioAtual);
            return RedirectToAction("Index", new { area = "Admin" }).Success("Usuário atualizado com sucesso.");
        }

        [HttpPost]
        public ActionResult Excluir(Usuario usuario)
        {
            var usuarioAtual = _usuario.BuscarPorId(usuario.UsuarioId);
            usuarioAtual.ExcluirUsuario();
            _usuario.Excluir(usuarioAtual);
            return RedirectToAction("Index", new { area = "Admin" }).Success("Usuário excluído com sucesso.");
        }

        public PartialViewResult AbrirModalAdicionar()
        {
            return PartialView("_Adicionar");
        }

        public PartialViewResult AbrirModalAtualizar(Guid usuarioId)
        {
            var usuario = _usuario.BuscarPorId(usuarioId);
            return PartialView("_Atualizar", usuario);
        }

        public PartialViewResult AbrirModalExcluir(Guid usuarioId)
        {
            var usuario = _usuario.BuscarPorId(usuarioId);
            return PartialView("_Excluir", usuario);
        }

        public ActionResult Ativo(Guid usuarioId, int pagina)
        {
            var usuarioAtual = _usuario.BuscarPorId(usuarioId);
            usuarioAtual.AtivarOuDesativar();
            _usuario.Atualizar(usuarioAtual);
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var usuarios = _usuario.BuscarTodos().Where(x => !x.Excluido).ToList(); ;
            var lista = new List<Usuario>();
            lista.AddRange(usuarios);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }

        public PartialViewResult BuscarUsuario(string textoDigitado, int pagina = 1)
        {
            TempData["Pagina"] = pagina;
            int itensPorPagina = 5;
            var usuarios = _usuario.BuscarTodosPorFiltro(textoDigitado).Where(x => !x.Excluido).ToList();
            var lista = new List<Usuario>();
            lista.AddRange(usuarios);
            return PartialView("_PartialTabela", lista.ToPagedList(pagina, itensPorPagina));
        }
    }
}


