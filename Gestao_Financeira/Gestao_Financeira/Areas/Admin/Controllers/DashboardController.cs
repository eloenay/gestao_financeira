﻿using Gestao_Financeira.Helpers;
using System.Web.Mvc;

namespace Gestao_Financeira.Areas.Admin.Controllers
{
    [Permissoes(Roles = "Administrador")]
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}