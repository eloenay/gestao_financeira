﻿using System.ComponentModel.DataAnnotations;

namespace Gestao_Financeira.Models
{
    public enum Status
    {
        Ativo = 1,
        Inativo = 2,

        [Display(Name = "Excluído")]
        Excluido = 3
    }

    public enum TipoUsuario
    {
        Administrador = 1,
        [Display(Name = "Usuário")]
        Usuario = 2
    }

    public enum TipoFinanceiro
    {
        Entrada = 1,
        [Display(Name = "Saída")]
        Saida = 2
    }
}