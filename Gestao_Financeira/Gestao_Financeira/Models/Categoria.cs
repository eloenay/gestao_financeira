﻿using Gestao_Financeira.Repository;
using Gestao_Financeira.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gestao_Financeira.Models
{
    public class Categoria
    {
        #region Atributos
        public Guid CategoriaId { get; set; }
        [Required]
        public string Nome { get; set; }
        public bool Ativo { get; set; } = true;
        public bool Excluido { get; set; } = false;
        public DateTime? DataAlteracao { get; set; }
        public DateTime DataRegistro { get; set; } = DateTime.UtcNow;
        #endregion

        #region Relacionamentos
        public Guid UsuarioId { get; set; } = Account.UsuarioId;
        public Usuario Usuario { get; set; }
        #endregion

        #region Bidirecional
        public ICollection<Financeiro> Financeiros { get; set; }
        #endregion

        #region Crud
        CategoriaRepositorio _categoriaRepositorio = new CategoriaRepositorio();
        public void Adicionar(Categoria categoria) => _categoriaRepositorio.Adicionar(categoria, $"Adicionado a categoria {categoria.Nome}");
        public void Atualizar(Categoria categoria) => _categoriaRepositorio.Atualizar(categoria, $"Alterado a categoria {categoria.Nome}");
        public void Excluir(Categoria categoria) => _categoriaRepositorio.Excluir(categoria, $"Excluido a categoria {categoria.Nome}");
        public Categoria BuscarPorId(Guid categoriaIdId) => _categoriaRepositorio.BuscarPorId(categoriaIdId);
        public IEnumerable<Categoria> BuscarTodos(Guid usuarioId) => _categoriaRepositorio.BuscarTodos(usuarioId);
        public IEnumerable<Categoria> BuscarTodosPorFiltro(string textoDigitado, Guid usuarioId) => _categoriaRepositorio.BuscarTodosPorFiltro(textoDigitado, usuarioId);
        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
            DataAlteracao = DateTime.UtcNow;
        }

        public void AlterarCategoria(Categoria categoria)
        {
            Nome = categoria.Nome;
            DataAlteracao = DateTime.UtcNow;
        }

        public void ExcluirCategoria()
        {
            DataAlteracao = DateTime.UtcNow;
            Excluido = true;
        }
        #endregion
    }
}