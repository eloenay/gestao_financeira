﻿using Gestao_Financeira.Repository;
using Gestao_Financeira.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gestao_Financeira.Models
{
    public class Usuario
    {
        #region Atributos
        public Guid UsuarioId { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string Senha { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
        public bool EmailConfirmado { get; set; } = false;
        public TipoUsuario TipoUsuario { get; set; }
        public bool Ativo { get; set; } = true;
        public bool Excluido { get; set; } = false;
        public DateTime? DataAlteracao { get; set; }
        public DateTime DataRegistro { get; set; } = DateTime.UtcNow;
        #endregion

        #region Bidirecional
        public ICollection<Categoria> Categorias { get; set; }
        public ICollection<Financeiro> Financeiros { get; set; }
        public ICollection<Log> Logs { get; set; }
        #endregion

        #region Crud
        UsuarioRepositorio _usuarioRepositorio = new UsuarioRepositorio();
        public void Adicionar(Usuario usuario) => _usuarioRepositorio.Adicionar(usuario, $"Adicionado o usuário {usuario.Nome} com o perfil {usuario.TipoUsuario.GetDisplayName()}");
        public void Atualizar(Usuario usuario) => _usuarioRepositorio.Atualizar(usuario, $"Atualizado o usuário {usuario.Nome} com o perfil {usuario.TipoUsuario.GetDisplayName()}");
        public void Excluir(Usuario usuario) => _usuarioRepositorio.Excluir(usuario, $"Excluido o usuário {usuario.Nome} com o perfil {usuario.TipoUsuario.GetDisplayName()}");
        public void Cadastro(Usuario usuario) => _usuarioRepositorio.Cadastro(usuario, $"Usuário {usuario.Nome} se cadastrou no dia {usuario.DataRegistro}");
        public Usuario BuscarPorId(Guid usuarioId) => _usuarioRepositorio.BuscarPorId(usuarioId);
        public Usuario BuscarPorLogin(string login) => _usuarioRepositorio.BuscarPorLogin(login);
        public IEnumerable<Usuario> BuscarTodos() => _usuarioRepositorio.BuscarTodos();
        public IEnumerable<Usuario> BuscarTodosPorFiltro(string textoDigitado) => _usuarioRepositorio.BuscarTodosPorFiltro(textoDigitado);

        #endregion

        #region Métodos
        public void AtivarOuDesativar()
        {
            Ativo = !Ativo;
            DataAlteracao = DateTime.UtcNow;
        }

        public void CriarUsuario(string senha)
        {
            TipoUsuario = TipoUsuario.Usuario;
            Senha = Hash.GerarHash(senha);
        }

        public void AlterarUsuario(Usuario usuario)
        {
            Nome = usuario.Nome;
            Login = usuario.Login;
            DataAlteracao = DateTime.UtcNow;
            if (usuario.Senha != null)
                Senha = Hash.GerarHash(usuario.Senha);
        }

        public void ExcluirUsuario()
        {
            DataAlteracao = DateTime.UtcNow;
            Excluido = true;
        }
        #endregion
    }
}