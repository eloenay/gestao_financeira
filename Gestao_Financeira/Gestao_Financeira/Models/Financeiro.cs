﻿using Gestao_Financeira.Repository;
using Gestao_Financeira.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gestao_Financeira.Models
{
    public class Financeiro
    {
        #region Atributos
        public Guid FinanceiroId { get; set; }
        [Required]
        public string Descricao { get; set; }
        [Required]
        public TipoFinanceiro TipoFinanceiro { get; set; }
        [Required]
        public DateTime DataLancamento { get; set; }
        public DateTime? DataAlteracao { get; set; }
        [Required]
        public decimal Valor { get; set; }
        public string ValorFormat => Valor.MarkValor();
        #endregion

        #region Relacionamentos
        [Required]
        public Guid CategoriaId { get; set; }
        public Categoria Categoria { get; set; }
        public Guid UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        #endregion

        #region Crud
        FinanceiroRepositorio _financeiroRepositories = new FinanceiroRepositorio();
        public void Adicionar(Financeiro financeiro) => _financeiroRepositories.Adicionar(financeiro, $"Adicionado financeiro {financeiro.Descricao} tipo {financeiro.TipoFinanceiro.GetDisplayName()}");
        public void Atualizar(Financeiro financeiro) => _financeiroRepositories.Atualizar(financeiro, $"Alterado financeiro {financeiro.Descricao} tipo {financeiro.TipoFinanceiro.GetDisplayName()}");
        public Financeiro BuscarPorId(Guid financeiroId) => _financeiroRepositories.BuscarPorId(financeiroId);
        public IEnumerable<Financeiro> BuscarTodos(Guid usuarioId) => _financeiroRepositories.BuscarTodos(usuarioId);
        public IEnumerable<Financeiro> BuscarTodosPorAnoMes(Guid usuarioId, int ano, int? mes) => _financeiroRepositories.BuscarTodosPorAnoMes(usuarioId, ano, mes);
        public IEnumerable<Financeiro> BuscarTodosPorFiltro(string textoDigitado, Guid categoria, string tipo, int data, int ano, Guid usuarioId) => _financeiroRepositories.BuscarTodosPorFiltro(textoDigitado, categoria, tipo, data, ano, usuarioId);

        #endregion

        #region Métodos
        public void EditarFinanceiro(string valor)
        {
            DataAlteracao = DateTimeFormatRO.GetDateTimeFusoRondonia(DateTime.UtcNow);
            Valor = Convert.ToDecimal(valor);
        }
        #endregion
    }
}