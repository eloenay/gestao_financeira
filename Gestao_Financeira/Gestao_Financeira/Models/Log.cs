﻿using Gestao_Financeira.Utils;
using System;

namespace Gestao_Financeira.Models
{
    public class Log
    {
        #region Atributos
        public Guid LogId { get; set; }
        public string TipoLog { get; set; }
        public string NomeUsuario { get; set; }
        public string TipoUsuario { get; set; }
        public string Descricao { get; set; }
        public DateTime DataRegistro { get; set; }
        #endregion

        #region Relacionamentos
        public Guid? UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        #endregion

        #region Métodos
        public Log CriarLog(string tipoLog, string descricao)
        {
            var log = new Log
            {
                LogId = Guid.NewGuid(),
                TipoLog = tipoLog,
                Descricao = descricao,
                DataRegistro = DateTime.UtcNow,
                NomeUsuario = Account.NomeCompleto,
                TipoUsuario = Account.Roles,
                UsuarioId = Account.UsuarioId
            };
            return log;
        }
        public Log CriarLogUserCadastro(string tipoLog, string descricao)
        {
            var log = new Log
            {
                LogId = Guid.NewGuid(),
                TipoLog = tipoLog,
                Descricao = descricao,
                DataRegistro = DateTime.UtcNow,
                NomeUsuario = Account.NomeCompleto,
                TipoUsuario = Account.Roles
            };
            return log;
        }
        #endregion
    }
}