﻿using Gestao_Financeira.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Gestao_Financeira.EntityConfig
{
    public class CategoriaConfig : EntityTypeConfiguration<Categoria>
    {
        public CategoriaConfig()
        {
            HasKey(x => x.CategoriaId);
            Property(x => x.CategoriaId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).IsRequired().HasMaxLength(100);
            Property(x => x.Ativo);
            Property(x => x.DataAlteracao).IsOptional();
            Property(x => x.DataRegistro).IsRequired();
            Property(x => x.Excluido);

            HasRequired(x => x.Usuario).WithMany(x => x.Categorias).HasForeignKey(x => x.UsuarioId).WillCascadeOnDelete(false);

            ToTable(nameof(Categoria));
        }
    }
}