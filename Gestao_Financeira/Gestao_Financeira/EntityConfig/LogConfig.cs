﻿using Gestao_Financeira.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Gestao_Financeira.EntityConfig
{
    public class LogConfig : EntityTypeConfiguration<Log>
    {
        public LogConfig()
        {
            HasKey(x => x.LogId);
            Property(x => x.LogId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.TipoLog).IsRequired().HasMaxLength(20);
            Property(x => x.Descricao).IsRequired().HasMaxLength(500);
            Property(x => x.DataRegistro).IsRequired();

            HasOptional(x => x.Usuario).WithMany(x => x.Logs).HasForeignKey(x => x.UsuarioId).WillCascadeOnDelete(false);

            ToTable(nameof(Log));
        }
    }
}