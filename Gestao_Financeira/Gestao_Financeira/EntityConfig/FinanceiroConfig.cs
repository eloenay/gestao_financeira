﻿using Gestao_Financeira.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Gestao_Financeira.EntityConfig
{
    public class FinanceiroConfig : EntityTypeConfiguration<Financeiro>
    {
        public FinanceiroConfig()
        {
            HasKey(x => x.FinanceiroId);
            Property(x => x.FinanceiroId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Descricao).IsRequired().HasMaxLength(250);
            Property(x => x.DataLancamento).IsRequired();
            Property(x => x.TipoFinanceiro).IsRequired();
            Property(x => x.Valor).IsRequired();
            Property(x => x.DataAlteracao).IsOptional();

            HasRequired(x => x.Usuario).WithMany(x => x.Financeiros).HasForeignKey(x => x.UsuarioId).WillCascadeOnDelete(false);
            HasRequired(x => x.Categoria).WithMany(x => x.Financeiros).HasForeignKey(x => x.CategoriaId).WillCascadeOnDelete(false);

            ToTable(nameof(Financeiro));
        }
    }
}