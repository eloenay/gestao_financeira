﻿using Gestao_Financeira.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Gestao_Financeira.Repository
{
    public class UsuarioRepositorio : BaseRepositorio<Usuario>
    {
        public Usuario BuscarPorId(Guid usuarioId)
        {
            return DbSet.FirstOrDefault(c => c.UsuarioId == usuarioId);
        }

        public Usuario BuscarPorLogin(string login)
        {
            return DbSet.FirstOrDefault(c => c.Login == login && !c.Excluido);
        }

        public IEnumerable<Usuario> BuscarTodos()
        {
            return DbSet.OrderBy(x => x.Nome);
        }

        public IEnumerable<Usuario> BuscarTodosPorFiltro(string textoDigitado)
        {
            return DbSet.Where(x => x.Nome.StartsWith(textoDigitado)).OrderBy(x => x.Nome);
        }
    }
}