﻿using Gestao_Financeira.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Gestao_Financeira.Repository
{
    public class CategoriaRepositorio : BaseRepositorio<Categoria>
    {
        public Categoria BuscarPorId(Guid categoriaId)
        {
            return DbSet.FirstOrDefault(c => c.CategoriaId == categoriaId);
        }

        public IEnumerable<Categoria> BuscarTodos(Guid usuarioId)
        {
            return DbSet.Where(x => x.UsuarioId == usuarioId).OrderBy(x => x.Nome);
        }

        public IEnumerable<Categoria> BuscarTodosPorFiltro(string textoDigitado, Guid usuarioId)
        {
                return DbSet.Include(x => x.Usuario).Where(x => x.Nome.StartsWith(textoDigitado) && x.UsuarioId == usuarioId).OrderBy(x => x.Nome);
        }
    }
}