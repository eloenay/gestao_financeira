﻿using Gestao_Financeira.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Gestao_Financeira.Repository
{
    public class FinanceiroRepositorio : BaseRepositorio<Financeiro>
    {
        public Financeiro BuscarPorId(Guid financeiroId)
        {
            return DbSet.Include(x => x.Categoria).FirstOrDefault(c => c.FinanceiroId == financeiroId);
        }

        public IEnumerable<Financeiro> BuscarTodos(Guid usuarioId)
        {
            return DbSet.Include(x => x.Categoria).Where(x => x.UsuarioId == usuarioId).OrderByDescending(x => x.DataLancamento);
        }

        public IEnumerable<Financeiro> BuscarTodosPorAnoMes(Guid usuarioId, int ano, int? mes)
        {
            var result = mes != null ? DbSet.Include(x => x.Categoria).Where(x => x.UsuarioId == usuarioId && x.DataLancamento.Year == ano && x.DataLancamento.Month == mes) : DbSet.Include(x => x.Categoria).Where(x => x.UsuarioId == usuarioId && x.DataLancamento.Year == ano);

            return result.OrderByDescending(x => x.DataLancamento);
        }


        public IEnumerable<Financeiro> BuscarTodosPorDescricao(string textoDigitado, Guid usuarioId)
        {
            return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.UsuarioId == usuarioId).OrderByDescending(x => x.DataLancamento);
        }

        public IEnumerable<Financeiro> BuscarTodosPorFiltro(string textoDigitado, Guid categoria, string tipo, int data, int ano, Guid usuarioId)
        {
            if (categoria != Guid.Empty && !string.IsNullOrEmpty(tipo) && data != 0)
            {//Todos
                var tipofinanceiro = tipo == "1" ? TipoFinanceiro.Entrada : TipoFinanceiro.Saida;
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.TipoFinanceiro == tipofinanceiro && x.UsuarioId == usuarioId && x.DataLancamento.Month == data && x.CategoriaId == categoria && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
            else if (categoria != Guid.Empty && !string.IsNullOrEmpty(tipo) && data == 0)
            {//Categoria e Tipo
                var tipofinanceiro = tipo == "1" ? TipoFinanceiro.Entrada : TipoFinanceiro.Saida;
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.TipoFinanceiro == tipofinanceiro && x.UsuarioId == usuarioId && x.CategoriaId == categoria && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
            else if (categoria != Guid.Empty && string.IsNullOrEmpty(tipo) && data != 0)
            {//Categoria e Data
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.UsuarioId == usuarioId && x.DataLancamento.Month == data && x.CategoriaId == categoria && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
            else if (categoria == Guid.Empty && !string.IsNullOrEmpty(tipo) && data != 0)
            {//Tipo e Data
                var tipofinanceiro = tipo == "1" ? TipoFinanceiro.Entrada : TipoFinanceiro.Saida;
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.TipoFinanceiro == tipofinanceiro && x.UsuarioId == usuarioId && x.DataLancamento.Month == data && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
            else if (categoria != Guid.Empty && string.IsNullOrEmpty(tipo) && data == 0)
            {//Somente Categoria
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.UsuarioId == usuarioId && x.CategoriaId == categoria && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
            else if (categoria == Guid.Empty && !string.IsNullOrEmpty(tipo) && data == 0)
            {//Somente Tipo
                var tipofinanceiro = tipo == "1" ? TipoFinanceiro.Entrada : TipoFinanceiro.Saida;
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.TipoFinanceiro == tipofinanceiro && x.UsuarioId == usuarioId && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);

            }
            else if (categoria == Guid.Empty && string.IsNullOrEmpty(tipo) && data != 0)
            {//Somente Data
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.UsuarioId == usuarioId && x.DataLancamento.Month == data && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
            else
            {
                return DbSet.Include(x => x.Categoria).Where(x => x.Descricao.StartsWith(textoDigitado) && x.UsuarioId == usuarioId && x.DataLancamento.Year == ano).OrderByDescending(x => x.DataLancamento);
            }
        }
    }
}