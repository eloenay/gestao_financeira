﻿using Gestao_Financeira.Context;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using System;
using System.Data.Entity;

namespace Gestao_Financeira.Repository
{
    public class BaseRepositorio<TEntity> : IDisposable where TEntity : class
    {
        protected ConexaoBanco Context;
        protected DbSet<TEntity> DbSet;
        private readonly Log _log;

        public BaseRepositorio()
        {
            Context = new ConexaoBanco();
            DbSet = Context.Set<TEntity>();
            _log = new Log();
        }

        public virtual TEntity Cadastro(TEntity entity, string descricao)
        {
            var log = _log.CriarLogUserCadastro("Create", descricao);
            Context.Entry(entity).State = EntityState.Added;
            Context.Entry(log).State = EntityState.Added;
            Context.SaveChanges();
            return entity;
        }

        public virtual TEntity Adicionar(TEntity entity, string descricao)
        {
            var log = _log.CriarLog("Create", descricao);
            Context.Entry(entity).State = EntityState.Added;
            Context.Entry(log).State = EntityState.Added;
            Context.SaveChanges();
            return entity;
        }

        public virtual TEntity Atualizar(TEntity entity, string descricao)
        {
            var log = _log.CriarLog("Update", descricao);
            Context.Entry(entity).State = EntityState.Modified;
            Context.Entry(log).State = EntityState.Added;
            Context.SaveChanges();
            return entity;
        }

        public virtual TEntity Excluir(TEntity entity, string descricao)
        {
            var log = _log.CriarLog("Delete", descricao);
            Context.Entry(entity).State = EntityState.Modified;
            Context.Entry(log).State = EntityState.Added;
            Context.SaveChanges();
            return entity;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Context.Dispose();
        }
    }
}