using Gestao_Financeira.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace Gestao_Financeira.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Gestao_Financeira.Context.ConexaoBanco>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Gestao_Financeira.Context.ConexaoBanco context)
        {
            context.Usuario.AddOrUpdate(u => u.Login,
                     new Usuario()
                     {
                         UsuarioId = Guid.NewGuid(),
                         Nome = "Administrador",
                         Login = "admin",
                         Senha = "8D969EEF6ECAD3C29A3A629280E686CFC3F5D5A86AFF3CA122C923ADC6C92",//senha = 123456
                         Email = "eloenay.pereira@gmail.com",
                         EmailConfirmado = true,
                         Ativo = true,
                         TipoUsuario = TipoUsuario.Administrador,
                         Excluido = false,
                         DataAlteracao = null,
                         DataRegistro = DateTime.Now.Date
                     }
                      //new Usuario()
                      //{
                      //    UsuarioId = Guid.NewGuid(),
                      //    Nome = "Usu�rio",
                      //    Login = "usuario",
                      //    Senha = "8D969EEF6ECAD3C29A3A629280E686CFC3F5D5A86AFF3CA122C923ADC6C92",//senha = 123456
                      //    Email = "eloenay.pereira@gmail.com",
                      //    EmailConfirmado = true,
                      //    Ativo = true,
                      //    TipoUsuario = TipoUsuario.Usuario,
                      //    Excluido = false,
                      //    DataAlteracao = null,
                      //    DataRegistro = DateTime.Now.Date
                      //}
                      );
        }
    }
}
