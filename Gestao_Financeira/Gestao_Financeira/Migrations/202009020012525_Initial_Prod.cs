﻿namespace Gestao_Financeira.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_Prod : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categoria",
                c => new
                    {
                        CategoriaId = c.Guid(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100, unicode: false),
                        Ativo = c.Boolean(nullable: false),
                        Excluido = c.Boolean(nullable: false),
                        DataAlteracao = c.DateTime(precision: 0),
                        DataRegistro = c.DateTime(nullable: false, precision: 0),
                        UsuarioId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.CategoriaId)
                .ForeignKey("dbo.Usuario", t => t.UsuarioId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Financeiro",
                c => new
                    {
                        FinanceiroId = c.Guid(nullable: false, identity: true),
                        Descricao = c.String(nullable: false, maxLength: 250, unicode: false),
                        TipoFinanceiro = c.Int(nullable: false),
                        DataLancamento = c.DateTime(nullable: false, precision: 0),
                        DataAlteracao = c.DateTime(precision: 0),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CategoriaId = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.FinanceiroId)
                .ForeignKey("dbo.Categoria", t => t.CategoriaId)
                .ForeignKey("dbo.Usuario", t => t.UsuarioId)
                .Index(t => t.CategoriaId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        UsuarioId = c.Guid(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 250, unicode: false),
                        Login = c.String(nullable: false, maxLength: 50, unicode: false),
                        Senha = c.String(nullable: false, maxLength: 1000, unicode: false),
                        Email = c.String(nullable: false, maxLength: 1000, unicode: false),
                        EmailConfirmado = c.Boolean(nullable: false),
                        TipoUsuario = c.Int(nullable: false),
                        Ativo = c.Boolean(nullable: false),
                        Excluido = c.Boolean(nullable: false),
                        DataAlteracao = c.DateTime(precision: 0),
                        DataRegistro = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        LogId = c.Guid(nullable: false),
                        TipoLog = c.String(nullable: false, maxLength: 20, unicode: false),
                        NomeUsuario = c.String(maxLength: 1000, unicode: false),
                        TipoUsuario = c.String(maxLength: 1000, unicode: false),
                        Descricao = c.String(nullable: false, maxLength: 500, unicode: false),
                        DataRegistro = c.DateTime(nullable: false, precision: 0),
                        UsuarioId = c.Guid(),
                    })
                .PrimaryKey(t => t.LogId)
                .ForeignKey("dbo.Usuario", t => t.UsuarioId)
                .Index(t => t.UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Categoria", "UsuarioId", "dbo.Usuario");
            DropForeignKey("dbo.Financeiro", "UsuarioId", "dbo.Usuario");
            DropForeignKey("dbo.Log", "UsuarioId", "dbo.Usuario");
            DropForeignKey("dbo.Financeiro", "CategoriaId", "dbo.Categoria");
            DropIndex("dbo.Log", new[] { "UsuarioId" });
            DropIndex("dbo.Financeiro", new[] { "UsuarioId" });
            DropIndex("dbo.Financeiro", new[] { "CategoriaId" });
            DropIndex("dbo.Categoria", new[] { "UsuarioId" });
            DropTable("dbo.Log");
            DropTable("dbo.Usuario");
            DropTable("dbo.Financeiro");
            DropTable("dbo.Categoria");
        }
    }
}
