﻿using Gestao_Financeira.Helpers;
using Gestao_Financeira.Models;
using Gestao_Financeira.Utils;
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Gestao_Financeira.Controllers
{
    public class HomeController : Controller
    {
        private readonly Usuario _usuario;

        public HomeController()
        {
            _usuario = new Usuario();
        }

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult AbrirModalLogin()
        {
            return PartialView("_ModalLogin");
        }

        [HttpPost]
        public ActionResult Cadastrar(Usuario usuario)
        {
            if (!ModelState.IsValid) return RedirectToAction("Index").Error("Erro ao realizar cadastro");
            if (_usuario.BuscarPorLogin(usuario.Login) != null) return RedirectToAction("Index").Error("Este login já está em uso");
            usuario.CriarUsuario(usuario.Senha);
            _usuario.Cadastro(usuario);
            var usuarioCadastrado = _usuario.BuscarPorLogin(usuario.Login);
            var identity = new ClaimsIdentity(new[]
            {
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider","ASP.NET Identity"),
                new Claim(ClaimTypes.NameIdentifier, usuarioCadastrado.UsuarioId.ToString()),
                new Claim(ClaimTypes.Authentication, usuarioCadastrado.Login),
                new Claim(ClaimTypes.Name, usuarioCadastrado.Nome),
                new Claim(ClaimTypes.Role, Enum.GetName(typeof(TipoUsuario), usuarioCadastrado.TipoUsuario)),
            }, "ApplicationCookie");
            Request.GetOwinContext().Authentication.SignIn(identity);
            return RedirectToAction("Index", "Dashboard", new { area = "User" }).Success($"Usuário Cadastrado, Bem vindo {usuarioCadastrado.Nome}");
        }

        [HttpPost]
        public ActionResult Entrar(Usuario usuarioTela)
        {
            try
            {
                if (usuarioTela == null) return RedirectToAction("Index", "Home").Error("Preencha todos os campos!");
                var usuario = _usuario.BuscarPorLogin(usuarioTela.Login);
                if (usuario == null) return RedirectToAction("Index", "Home").Error("Este usuário não existe!");
                if (usuario.Ativo != true) return RedirectToAction("Index", "Home").Error("Este usuário está desativado!");
                if (usuario.Senha != Hash.GerarHash(usuarioTela.Senha)) return RedirectToAction("Index", "Home").Error("Senha incorreta!");

                var identity = new ClaimsIdentity(new[]
                {
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider","ASP.NET Identity"),
                new Claim(ClaimTypes.NameIdentifier, usuario.UsuarioId.ToString()),
                new Claim(ClaimTypes.Authentication, usuario.Login),
                new Claim(ClaimTypes.Name, usuario.Nome),
                new Claim(ClaimTypes.Role, Enum.GetName(typeof(TipoUsuario), usuario.TipoUsuario)),
            }, "ApplicationCookie");
                Request.GetOwinContext().Authentication.SignIn(identity);

                if (usuario.TipoUsuario == TipoUsuario.Administrador)
                    return RedirectToAction("Index", "Dashboard", new { area = "Admin" }).Information($"Bem vindo {usuario.Nome}");
                else
                    return RedirectToAction("Index", "Dashboard", new { area = "User" }).Information($"Bem vindo {usuario.Nome}");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home").Error("Erro ao fazer login!");
            }
        }

        public ActionResult Logout()
        {
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}