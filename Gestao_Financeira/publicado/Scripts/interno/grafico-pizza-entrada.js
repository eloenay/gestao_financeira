﻿$(function () {
    "use string";

    window.GraficoPizzaEntrada = window.GraficoPizzaEntrada || {};

    var graficoPE = $("#graficopizzaentrada");

    GraficoPizzaEntrada.Carregar = function () {
        $(document).ready(function () {
            GraficoPizzaEntrada.BuscarInformacoes();
        });
    };

    GraficoPizzaEntrada.BuscarInformacoes = function (ano = $("#graficogeralporano").data("anoinicio"), mes = graficoPE.data("mes")) {
        
        graficoPE.LoadingOverlay("show");
        $.ajax({
            url: graficoPE.data("url"),
            data: { mes: mes, ano: ano },
            success: function (result) {
                GraficoPizzaEntrada.MontarGrafico(result, mes, ano);
                graficoPE.LoadingOverlay("hide");
            }
        });
    };

    GraficoPizzaEntrada.MontarGrafico = function (result, mes, ano) {
        Highcharts.chart('graficopizzaentrada', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: `Gráfico de entrada, ${GraficoPizzaEntrada.Mes(mes)}, ${ano}`
            },
            tooltip: {
                pointFormat: '{series.name}: <b>R$ {point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Total',
                colorByPoint: true,
                data: result
            }]
        });
    };

    GraficoPizzaEntrada.Mes = function (mes) {
        mes = parseInt(mes);
        switch (mes) {
            case 1:
                return "Janeiro";
            case 2:
                return "Fevereiro";
            case 3:
                return "Março";
            case 4:
                return "Abril";
            case 5:
                return "Maio";
            case 6:
                return "Junho";
            case 7:
                return "Julho";
            case 8:
                return "Agosto";
            case 9:
                return "Setembro";
            case 10:
                return "Outubro";
            case 11:
                return "Novembro";
            case 12:
                return "Dezembro";
            default:
        }
    };

    GraficoPizzaEntrada.Carregar();
});